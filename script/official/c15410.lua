--勇炎星－エンショウ
--Brotherhood of the Fire Fist - Gorilla
local s, id = GetID()
function s.initial_effect(c)
	--tribute
	local e1 = Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(id, 0))
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCost(s.setcost)
	e1:SetTarget(s.settg)
	e1:SetOperation(s.setop)
	c:RegisterEffect(e1)
	--destroy
	local e2 = Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(id, 1))
	e2:SetProperty(EFFECT_FLAG_CARD_TARGET+EFFECT_FLAG_DELAY)
	e2:SetCategory(CATEGORY_DESTROY)
	e2:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_TRIGGER_O)
	e2:SetCode(EVENT_SUMMON_SUCCESS)
	e2:SetCost(s.descost)
	e2:SetTarget(s.destg)
	e2:SetOperation(s.desop)
	c:RegisterEffect(e2)
	local e3 = e2:Clone()
	e3:SetCode(EVENT_SPSUMMON_SUCCESS)
	c:RegisterEffect(e3)
end

function s.descost(e, tp, eg, ep, ev, re, r, rp, chk)
	local nc = Duel.IsExistingMatchingCard(s.filterdes1, tp, LOCATION_ONFIELD, 0, 1, nil)
	if chk == 0 then
		return nc
	end
	if nc then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
		local g = Duel.SelectMatchingCard(tp, s.filterdes1, tp, LOCATION_ONFIELD, 0, 1, 1, nil)
		Duel.SendtoGrave(g, REASON_COST)
	end
end
function s.filterdes2(c)
	return true
end
function s.filterdes1(c)
	return c:IsFaceup() and c:IsSetCard(0x7c) and c:IsType(TYPE_SPELL + TYPE_TRAP) and c:IsAbleToGraveAsCost() and
		Duel.IsExistingTarget(s.filterdes2, 0, 0, LOCATION_ONFIELD, 1, c)
end
function s.destg(e, tp, eg, ep, ev, re, r, rp, chk, chkc)
	if chkc then
		return chkc:IsOnField() and s.filterdes2(chkc) and chkc:IsControler(1-tp)
	end
	if chk == 0 then
		return Duel.IsExistingTarget(s.filterdes2, tp, 0, LOCATION_ONFIELD, 1, nil)
	end
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_DESTROY)
	local g2 = Duel.SelectTarget(tp, s.filterdes2, tp, 0, LOCATION_ONFIELD, 1, 1, nil)
	Duel.SetOperationInfo(0, CATEGORY_DESTROY, g2, 1, 0, 0)
end
function s.desop(e, tp, eg, ep, ev, re, r, rp)
	local tc = Duel.GetFirstTarget()
	if tc:IsRelateToEffect(e) then
		Duel.Destroy(tc, REASON_EFFECT)
	end
end

function s.setcost(e, tp, eg, ep, ev, re, r, rp, chk)
	if chk == 0 then
		return e:GetHandler():IsReleasable()
	end
	Duel.Release(e:GetHandler(), REASON_COST)
end
function s.filter(c, typeOf, dontCheck)
	local passType = TYPE_SPELL
	if (typeOf == TYPE_SPELL) then
		passType = TYPE_TRAP
	end
	return c:IsSetCard(0x7c) and c:IsType(typeOf) and c:IsSSetable() and
		(dontCheck or Duel.IsExistingMatchingCard(s.filter2, tp, LOCATION_DECK, 0, 1, nil, c, passType))
end
function s.filter2(c, reserved, typeOf)
	return c:IsSetCard(0x7c) and c:IsType(typeOf) and c:IsSSetable() and c ~= reserved
end
function s.settg(e, tp, eg, ep, ev, re, r, rp, chk)
	if chk == 0 then
		return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil, TYPE_SPELL,true) or
			Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil, TYPE_TRAP,true) and
				Duel.GetLocationCount(tp, LOCATION_SZONE) > 0
	end
end
function s.setop(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SET)
	local g1 = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK, 0, 1, 1, nil, TYPE_SPELL + TYPE_TRAP, true)
	-- if #g1 < 1 then
	-- 	return
	-- end
	-- local c1 = g1:GetFirst()
	-- local chkType = TYPE_SPELL
	-- local descNum = 3
	-- if (Card.IsType(c1, chkType)) then
	-- 	chkType = TYPE_TRAP
	-- 	descNum = 2
	-- end
	-- local pickSecond = Duel.SelectYesNo(tp, aux.Stringid(id, descNum))
	-- local g2 = Group.CreateGroup()
	-- if
	-- 	pickSecond and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil, chkType, true) and
	-- 		Duel.GetLocationCount(tp, LOCATION_SZONE) > 1
	--  then
	-- 	g2 = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK, 0, 1, 1, nil, chkType, true)
	-- end
	-- g1:Merge(g2)
	if #g1 > 0 then
		Duel.SSet(tp, g1)
		Duel.ConfirmCards(1-tp,g1)
	end
end
