local s, id = GetID()

function s.initial_effect(c)
	local e4 = Effect.CreateEffect(c)
	e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
	e4:SetType(EFFECT_TYPE_SINGLE)
	e4:SetCode(EFFECT_SPSUMMON_CONDITION)
	e4:SetValue(aux.FALSE)
	--c:RegisterEffect(e4)
	--spsummon
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_UNCOPYABLE)
	e1:SetCode(EFFECT_SPSUMMON_PROC)
	e1:SetRange(LOCATION_HAND)
	e1:SetCondition(s.hspcon)
	c:RegisterEffect(e1)
	--inc level
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_IGNITION)
	e2:SetRange(LOCATION_MZONE)
	e2:SetCountLimit(1)
	e2:SetCost(s.inccost)
	e2:SetOperation(s.incop)
	c:RegisterEffect(e2)
	--to grave
	local e5 = Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_CONTINUOUS)
	e5:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e5:SetCode(EVENT_TO_GRAVE)
	e5:SetOperation(s.regop)
	c:RegisterEffect(e5)
end

function s.regop(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetHandler()
	if c:IsPreviousLocation(LOCATION_ONFIELD) then
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_TRIGGER_O)
		e1:SetCode(EVENT_PHASE + PHASE_END)
		e1:SetRange(LOCATION_GRAVE)
		e1:SetProperty(EFFECT_FLAG_DELAY)
		e1:SetCountLimit(1,id)
		e1:SetTarget(s.thtg)
		e1:SetOperation(s.thop)
		e1:SetReset(RESET_EVENT + 0x1fe0000 + RESET_PHASE + PHASE_END)
		c:RegisterEffect(e1)
	end
end
function s.thtg(e, tp, eg, ep, ev, re, r, rp, chk)
	if chk == 0 then
		return Duel.GetLocationCount(tp, LOCATION_SZONE) > 0 and
			Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil) and
			Duel.IsExistingMatchingCard(Card.IsDiscardable, tp, LOCATION_HAND, 0, 1, nil)
	end
end
function s.thop(e, tp, eg, ep, ev, re, r, rp)
	if not Duel.IsExistingMatchingCard(Card.IsDiscardable, tp, LOCATION_HAND, 0, 1, nil) then
		return
	end
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_DISCARD)
	if Duel.DiscardHand(tp, Card.IsDiscardable, 1, 1, REASON_EFFECT, nil) < 1 then
		return
	end
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SET)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK, 0, 1, 1, nil)
	if g:GetCount() > 0 then
		Duel.SSet(tp, g)
		Duel.ConfirmCards(1 - tp, g)
	end
end

function s.filter(c)
	return c:IsSetCard(0x7c) and c:IsType(TYPE_SPELL + TYPE_TRAP) and c:IsSSetable()
end

function s.hspcon(e, c)
	if c == nil then
		return true
	end
	return Duel.GetLocationCount(c:GetControler(), LOCATION_MZONE) > 0
end

function s.costfilter(c)
	return c:IsSetCard(0x7c) and c:IsAbleToGraveAsCost() and c:IsFaceup()
end

function s.inccost(e, tp, eg, ep, ev, re, r, rp, chk)
	if chk == 0 then
		return Duel.IsExistingMatchingCard(s.costfilter, tp, LOCATION_ONFIELD, 0, 1, nil)
	end
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_REMOVE)
	local g = Duel.SelectMatchingCard(tp, s.costfilter, tp, LOCATION_ONFIELD, 0, 1, 5, nil)
	if Duel.SendtoGrave(g, REASON_COST) > 0 then
		e:SetLabel(#g)
	end
end

function s.incop(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetHandler()
	local lv = e:GetLabel()
	if e:GetHandler():IsRelateToEffect(e) and lv > 0 then
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_LEVEL)
		e1:SetValue(lv)
		e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
		e1:SetRange(LOCATION_MZONE)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD_DISABLE)
		c:RegisterEffect(e1)
	end
end

function s.synlimit(e, c)
	if not c then
		return false
	end
	return not c:IsAttribute(ATTRIBUTE_FIRE)
end

function s.spfilter(c, ft)
	return c:IsFaceup() and c:IsSetCard(0x7c) and c:IsType(TYPE_SPELL + TYPE_TRAP) and c:IsAbleToGraveAsCost()
end
function s.spcost(e, tp, eg, ep, ev, re, r, rp, chk)
	local ft = Duel.GetLocationCount(tp, LOCATION_MZONE)
	if chk == 0 then
		return ft > 0 and Duel.IsExistingMatchingCard(s.spfilter, tp, LOCATION_ONFIELD, 0, 1, nil)
	end
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local g = Duel.SelectMatchingCard(tp, s.spfilter, tp, LOCATION_ONFIELD, 0, 1, 1, nil, ft)
	Duel.SendtoGrave(g, REASON_COST)
end
function s.sptg(e, tp, eg, ep, ev, re, r, rp, chk)
	if chk == 0 then
		return e:GetHandler():IsCanBeSpecialSummoned(e, 0, tp, true, false)
	end
	Duel.SetOperationInfo(0, CATEGORY_SPECIAL_SUMMON, e:GetHandler(), 1, 0, 0)
end
function s.spop(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetHandler()
	if not c:IsRelateToEffect(e) then
		return
	end
	Duel.SpecialSummon(c, 0, tp, tp, true, false, POS_FACEUP)
end
