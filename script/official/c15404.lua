--炎帝近衛兵
local s, id = GetID()
function s.initial_effect(c)
	--to deck and draw
	local e1 = Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(id, 0))
	e1:SetCategory(CATEGORY_TODECK)
	e1:SetType(EFFECT_TYPE_TRIGGER_O+ EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_CARD_TARGET+EFFECT_FLAG_DELAY)
	e1:SetCode(EVENT_SUMMON_SUCCESS)
	e1:SetTarget(s.tg)
	e1:SetOperation(s.op)
	c:RegisterEffect(e1)
	local e2 = e1:Clone()
	e2:SetCode(EVENT_SPSUMMON_SUCCESS)
	c:RegisterEffect(e2)
end
function s.shufflefilter(c)
	return c:IsSetCard(0x79) and c:IsType(TYPE_MONSTER) and c:IsAbleToDeck()
end
function s.setfilter(c)
	return c:IsSetCard(0x7c) and c:IsType(TYPE_SPELL + TYPE_TRAP) and c:IsSSetable()
end
function s.tg(e, tp, eg, ep, ev, re, r, rp, chk, chkc)
	if chkc then
		return false
	end
	if chk == 0 then
		return Duel.GetLocationCount(tp, LOCATION_SZONE) > 0 and
			Duel.IsExistingMatchingCard(s.setfilter, tp, LOCATION_GRAVE, 0, 1, nil) and
			Duel.IsExistingTarget(s.shufflefilter, tp, LOCATION_GRAVE, 0, 3, nil)
	end
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local g = Duel.SelectTarget(tp, s.shufflefilter, tp, LOCATION_GRAVE, 0, 3, 3, nil)
	Duel.SetOperationInfo(0, CATEGORY_TODECK, g, #g, 0, 0)
end
function s.op(e, tp, eg, ep, ev, re, r, rp)
	local tg = Duel.GetChainInfo(0, CHAININFO_TARGET_CARDS)
	if not tg or tg:FilterCount(Card.IsRelateToEffect, nil, e) ~= 3 then
		return
	end
	Duel.SendtoDeck(tg, nil, 0, REASON_EFFECT)
	local g = Duel.GetOperatedGroup()
	if g:IsExists(Card.IsLocation, 1, nil, LOCATION_DECK) then
		Duel.ShuffleDeck(tp)
	end
	local ct = g:FilterCount(Card.IsLocation, nil, LOCATION_DECK + LOCATION_EXTRA)
	if
		ct == 3 and Duel.GetLocationCount(tp, LOCATION_SZONE) > 0 and
			Duel.IsExistingMatchingCard(s.setfilter, tp, LOCATION_GRAVE, 0, 1, nil)
	 then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SET)
		local g = Duel.SelectMatchingCard(tp, s.setfilter, tp, LOCATION_GRAVE, 0, 1, 1, nil)
		if g:GetCount() > 0 then
			Duel.SSet(tp, g)
			Duel.ConfirmCards(1 - tp, g)
		end
	end
end
