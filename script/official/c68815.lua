local s,id=GetID()
function s.initial_effect(c)
    --shuffle and draw
	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(id,0))
	e2:SetCategory(CATEGORY_DRAW+CATEGORY_TODECK)
	e2:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e2:SetProperty(EFFECT_FLAG_DELAY+EFFECT_FLAG_PLAYER_TARGET)
	e2:SetCode(EVENT_SUMMON_SUCCESS)
	e2:SetTarget(s.drawtg)
	e2:SetOperation(s.drawop)
	c:RegisterEffect(e2)
	local e5=e2:Clone()
	e5:SetCode(EVENT_SPSUMMON_SUCCESS)
	c:RegisterEffect(e5)
   	--destroy
	local e4=Effect.CreateEffect(c)
	e4:SetDescription(aux.Stringid(id,1))
	e4:SetCategory(CATEGORY_DESTROY)
	e4:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e4:SetProperty(EFFECT_FLAG_DELAY)
	e4:SetCode(EVENT_BATTLE_START)
	e4:SetCondition(s.descon)
	e4:SetTarget(s.destg)
	e4:SetOperation(s.desop)
	c:RegisterEffect(e4)
end

function s.drawtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsPlayerCanDraw(tp,1)
		and Duel.IsExistingMatchingCard(Card.IsAbleToDeck,tp,LOCATION_HAND,0,1,nil) end
	Duel.SetTargetPlayer(tp)
	Duel.SetOperationInfo(0,CATEGORY_TODECK,nil,1,tp,LOCATION_HAND)
end
function s.drawop(e,tp,eg,ep,ev,re,r,rp)
	local p=Duel.GetChainInfo(0,CHAININFO_TARGET_PLAYER)
	Duel.Hint(HINT_SELECTMSG,p,HINTMSG_TODECK)
	local g=Duel.SelectMatchingCard(p,Card.IsAbleToDeck,p,LOCATION_HAND,0,1,63,nil)
	if #g==0 then return end
	Duel.SendtoDeck(g,nil,2,REASON_EFFECT)
	Duel.ShuffleDeck(p)
	Duel.BreakEffect()
	Duel.Draw(p,#g+1,REASON_EFFECT)
end


function s.descon(e,tp,eg,ep,ev,re,r,rp)
	local at = Duel.GetAttacker()
	local bc= at:GetBattleTarget()
	return bc
end
function s.destg(e,tp,eg,ep,ev,re,r,rp,chk)
	local at = Duel.GetAttacker()
	local bt = at:GetBattleTarget()
	local c = e:GetHandler()
	local a,b
	if at==c then 
		a=at 
		b=bt
	else
		a=bt
		b=at
	end
	if chk==0 then return true end
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,b,1,0,0)
	Duel.SetOperationInfo(0,CATEGORY_TODECK,a,1,0,0)
end
function s.desop(e,tp,eg,ep,ev,re,r,rp)
	local at = Duel.GetAttacker()
	local bt = at:GetBattleTarget()
	local c =e:GetHandler()
	local a,b
	if at==c then 
		a=at 
		b=bt
	else
		a=bt
		b=at
	end
	if Duel.SendtoDeck(a, nil, 2, REASON_EFFECT)>0 then
		Duel.Destroy(b, REASON_EFFECT)
	end
end

