--竜星の九支
local s,id=GetID()
function s.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_NEGATE)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_CHAINING)
	e1:SetCondition(s.condition)
	e1:SetTarget(s.target)
	e1:SetOperation(s.activate)
	c:RegisterEffect(e1)
end

function s.filter(c)
    return c:IsFaceup() and c:IsType(TYPE_MONSTER) and not c:IsSetCard(0x62)
        and c:IsAbleToRemove()
end

function s.condition(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local rc=re:GetHandler()
    e:SetLabelObject(rc)
	return  Duel.IsChainNegatable(ev) and re:GetHandler():GetControler()~=tp
		and re:IsActiveType(TYPE_MONSTER) and re:GetActivateLocation()==LOCATION_MZONE
        and not Duel.IsExistingMatchingCard(s.filter,tp,LOCATION_MZONE,0,1,nil)
end

function s.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetOperationInfo(0,CATEGORY_NEGATE,eg,1,0,0)
end
function s.activate(e,tp,eg,ep,ev,re,r,rp)
    --during this end phase your opponent draws 1 card
    local e0=Effect.CreateEffect(e:GetHandler())
    e0:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
    e0:SetCode(EVENT_PHASE+PHASE_END)
	e0:SetDescription(aux.Stringid(id,0))
    e0:SetCountLimit(1)
    e0:SetOperation(s.drawop)
    e0:SetReset(RESET_PHASE+PHASE_END)
    Duel.RegisterEffect(e0,tp)
    local rc = e:GetLabelObject()
    if not rc then return end
	if Duel.NegateEffect(ev) and rc:IsRelateToEffect(re) and Duel.Remove(rc,0,REASON_EFFECT+REASON_TEMPORARY)~=0 then
		local fid=e:GetHandler():GetFieldID()
		rc:RegisterFlagEffect(fid,RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END,0,1)
		local e1=Effect.CreateEffect(e:GetHandler())
		e1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		e1:SetCode(EVENT_PHASE+PHASE_END)
		e1:SetReset(RESET_PHASE+PHASE_END)
		e1:SetDescription(aux.Stringid(id,1))
		e1:SetLabelObject(rc)
		e1:SetLabel(fid)
		e1:SetCountLimit(1)
		e1:SetCondition(s.retcon)
		e1:SetOperation(s.retop)
		Duel.RegisterEffect(e1,tp)
	end
end

--Flag been raised
function s.retcon(e,tp,eg,ep,ev,re,r,rp)
	local fid = e:GetLabel()
	return e:GetLabelObject():GetFlagEffect(fid)~=0
end
	--Return banished monster
function s.retop(e,tp,eg,ep,ev,re,r,rp)
	Duel.ReturnToField(e:GetLabelObject())
end

function s.drawop(e,tp,eg,ep,ev,re,r,rp)
    Duel.Draw(1-tp,1,REASON_EFFECT)
end