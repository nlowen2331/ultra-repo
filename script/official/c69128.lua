
local s,id=GetID()
function s.initial_effect(c)
	c:EnableUnsummonable()
	--spsummon
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_SPSUMMON_PROC)
	e1:SetProperty(EFFECT_FLAG_UNCOPYABLE)
	e1:SetRange(LOCATION_HAND)
	e1:SetCondition(s.hspcon)
	e1:SetTarget(s.hsptg)
	e1:SetOperation(s.hspop)
	c:RegisterEffect(e1)
    --from grve sp
	local e4=Effect.CreateEffect(c)
	e4:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e4:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e4:SetType(EFFECT_TYPE_IGNITION)
	e4:SetRange(LOCATION_MZONE)
	e4:SetCountLimit(1,id)
	e4:SetTarget(s.sptg2)
	e4:SetOperation(s.spop2)
	c:RegisterEffect(e4)
    --destroy replace
	local e5=Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_SINGLE)
	e5:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e5:SetCode(EFFECT_DESTROY_REPLACE)
	e5:SetCountLimit(1)
	e5:SetRange(LOCATION_MZONE)
	e5:SetTarget(s.desreptg)
	c:RegisterEffect(e5)
end

function s.hspfilter(c)
    return c:IsSetCard(0x2016) and c:IsFaceup() and c:IsDestructable()
end

function s.rescon(sg,e,tp,mg)
	return sg:IsExists(Card.IsLocation,1,nil,LOCATION_MZONE)
end
function s.hspcon(e,c)
	if c==nil then return true end
	local tp = e:GetHandlerPlayer()
	local ft = Duel.GetLocationCount(tp, LOCATION_MZONE)
	local g = Duel.GetMatchingGroup(s.hspfilter, tp, LOCATION_ONFIELD, 0, nil,tp)
	if ft>0 then return #g>1 end
	return aux.SelectUnselectGroup(g,1,tp,2,2,s.rescon,0,tp)
end
function s.hsptg(e,tp,eg,ep,ev,re,r,rp,c)
	local g = Duel.GetMatchingGroup(s.hspfilter, tp, LOCATION_ONFIELD, 0, nil,tp)
	local ft = Duel.GetLocationCount(tp, LOCATION_MZONE)
	local sg = nil
	if ft>0 then
		sg = g:Select(tp, 2, 2,nil)
	else
		sg = aux.SelectUnselectGroup(g,1,tp,2,2,s.rescon,1,tp)
	end
	if g then
		sg:KeepAlive()
		e:SetLabelObject(sg)
	return true
	end
	return false
end
function s.hspop(e,tp,eg,ep,ev,re,r,rp,c)
	local g=e:GetLabelObject()
	if not g then return end
	Duel.Destroy(g, REASON_COST)
	g:DeleteGroup()
end

function s.spfilter2(c,e,tp)
	return c:IsLevelBelow(4) and c:IsSetCard(0x2016) and c:IsCanBeSpecialSummoned(e,0,tp,false,false)
end
function s.sptg2(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsControler(tp) and chkc:IsLocation(LOCATION_GRAVE) and s.spfilter2(chkc,e,tp) end
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0
		and Duel.IsExistingTarget(s.spfilter2,tp,LOCATION_GRAVE,0,1,nil,e,tp) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectTarget(tp,s.spfilter2,tp,LOCATION_GRAVE,0,1,1,nil,e,tp)
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,g,1,0,0)
end
function s.spop2(e,tp,eg,ep,ev,re,r,rp)
	local c = e:GetHandler()
	local tc=Duel.GetFirstTarget()
    if tc and tc:IsRelateToEffect(e) and Duel.SpecialSummonStep(tc,0,tp,tp,false,false,POS_FACEUP)~=0 then
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_LEAVE_FIELD_REDIRECT)
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
		e1:SetReset(RESET_EVENT+RESETS_REDIRECT)
		e1:SetValue(LOCATION_REMOVED)
		tc:RegisterEffect(e1,true)
		local e3=Effect.CreateEffect(e:GetHandler())
		e3:SetType(EFFECT_TYPE_SINGLE)
		e3:SetCode(EFFECT_DISABLE)
		e3:SetReset(RESET_EVENT+0x1fe0000)
		tc:RegisterEffect(e3,true)
		local e2=Effect.CreateEffect(e:GetHandler())
		e2:SetType(EFFECT_TYPE_SINGLE)
		e2:SetCode(EFFECT_DISABLE_EFFECT)
		e2:SetReset(RESET_EVENT+0x1fe0000)
		tc:RegisterEffect(e2,true)
		Duel.SpecialSummonComplete()
	end
end


function s.repfilter(c)
	return c:IsAbleToRemove() and c:IsSetCard(0x2016)
end
function s.desreptg(e,tp,eg,ep,ev,re,r,rp,chk)
	local c=e:GetHandler()
	if chk==0 then	return not c:IsReason(REASON_REPLACE) and (c:IsReason(REASON_BATTLE) or rp~=tp)
		and Duel.IsExistingMatchingCard(s.repfilter,tp,LOCATION_GRAVE,0,3,c) end
	if Duel.SelectYesNo(tp,aux.Stringid(id,0)) then
        local result= Auxiliary.SpeedroidMonsterTossDice(tp,1)
        if result<3 then return false end
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_DESREPLACE)
		local g=Duel.SelectMatchingCard(tp,s.repfilter,tp,LOCATION_GRAVE,0,3,3,c)
		Duel.Remove(g,POS_FACEUP,REASON_EFFECT)
		return true
	else return false end
end

function s.rdcon(e,tp,eg,ep,ev,re,r,rp)
	return ep~=tp
end
function s.rdop(e,tp,eg,ep,ev,re,r,rp)
	Duel.DoubleBattleDamage(ep)
end