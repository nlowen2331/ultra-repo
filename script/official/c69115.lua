--EMオッドアイズ・ミノタウロス
local s,id=GetID()
function s.initial_effect(c)
	--atk down
	local e2=Effect.CreateEffect(c)
	e2:SetCategory(CATEGORY_ATKCHANGE+CATEGORY_DEFCHANGE)
	e2:SetType(EFFECT_TYPE_TRIGGER_O+EFFECT_TYPE_FIELD)
	e2:SetCode(EVENT_PRE_DAMAGE_CALCULATE)
	e2:SetRange(LOCATION_HAND)
	e2:SetCondition(s.atkcon)
	e2:SetCost(s.atkcost)
	e2:SetOperation(s.atkop)
	c:RegisterEffect(e2)
end

function s.atkcost(e, tp, eg, ep, ev, re, r, rp, chk)
    local c = e:GetHandler()
    if chk == 0 then
        return not c:IsPublic()
    end
end

function s.atkcon(e,tp,eg,ep,ev,re,r,rp)
	e:SetLabelObject(nil)
	local a=Duel.GetAttacker()
	local d=Duel.GetAttackTarget()
	if a:IsControler(1-tp) then a,d=d,a end
	e:SetLabelObject(a)
	return a and a:IsControler(tp) and a:IsLocation(LOCATION_MZONE) and a:IsFaceup() and d and a:IsSetCard(0x2016)
end
function s.atktg(e,tp,eg,ep,ev,re,r,rp,chk)
    if chk==0 then return Duel.IsExistingMatchingCard(Card.IsDiscardable, tp, LOCATION_HAND, 0, 1, nil) 
        and Duel.GetFlagEffect(tp, id)==0
    end
    Duel.RegisterFlagEffect(tp, id, RESET_PHASE+PHASE_DAMAGE_CAL, 0, 1)
end
function s.atkop(e,tp,eg,ep,ev,re,r,rp)
	local tc = e:GetLabelObject()
	if tc:IsRelateToBattle() and tc:IsFaceup() then
		local e1=Effect.CreateEffect(e:GetHandler())
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetReset(RESET_PHASE+PHASE_DAMAGE_CAL)
		e1:SetValue(700)
		tc:RegisterEffect(e1)
        local e2 = e1:Clone()
        e2:SetCode(EFFECT_UPDATE_DEFENSE)
        tc:RegisterEffect(e2)
        local result= Auxiliary.SpeedroidMonsterTossDice(tp,1)
        if result<=3 then
            Duel.SendtoGrave(e:GetHandler(), REASON_EFFECT+REASON_DISCARD)
        end
	end
end
