local s,id=GetID()

function s.initial_effect(c)
    --xyz summon
	Xyz.AddProcedure(c,aux.FilterBoolFunctionEx(Card.IsSetCard,0x71),5,2,nil,nil,99)
	c:EnableReviveLimit()
    --If this card battles an opponent's monster, at the start of the Damage Step: You can detach 1 material from this card; shuffle that opponent's monster into the Deck
    local e1=Effect.CreateEffect(c)
    e1:SetDescription(aux.Stringid(id,0))
    e1:SetCategory(CATEGORY_TODECK)
    e1:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
    e1:SetCode(EVENT_BATTLE_START)
    e1:SetCondition(s.tdcon)
    e1:SetCost(s.tdcost)
    e1:SetTarget(s.tdtg)
    e1:SetOperation(s.tdop)
    c:RegisterEffect(e1,false,REGISTER_FLAG_DETACH_XMAT)
    --This card cannot be destroyed by your opponent's card effects while it has material.
    local e2=Effect.CreateEffect(c)
    e2:SetType(EFFECT_TYPE_SINGLE)
    e2:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
    e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
    e2:SetRange(LOCATION_MZONE)
    e2:SetCondition(s.indcon)
    e2:SetValue(1)
    c:RegisterEffect(e2)
end
function s.matfilter(c,xyz,sumtype,tp)
	return c:IsSetCard(0x71)
end

function s.tdcon(e,tp,eg,ep,ev,re,r,rp)
	local at = Duel.GetAttacker()
	local bc= at:GetBattleTarget()
	if not bc then return false end
    if bc:IsControler(tp) then e:SetLabelObject(at) else e:SetLabelObject(bc) end
	return true
end

function s.tdcost(e,tp,eg,ep,ev,re,r,rp,chk)
    if chk==0 then return e:GetHandler():CheckRemoveOverlayCard(tp,1,REASON_COST) end
    e:GetHandler():RemoveOverlayCard(tp,1,1,REASON_COST)
end

function s.tdtg(e,tp,eg,ep,ev,re,r,rp,chk)
    if chk==0 then return true end
    Duel.SetOperationInfo(0,CATEGORY_TODECK,e:GetLabelObject(),1,0,0)
end

function s.tdop(e,tp,eg,ep,ev,re,r,rp)
	local tc=e:GetLabelObject()
	if tc and tc:IsRelateToBattle() then
		Duel.SendtoDeck(tc, nil, 2, REASON_EFFECT)
	end
end

function s.indcon(e)
    return e:GetHandler():GetOverlayCount()>0
end