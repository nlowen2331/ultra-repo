
local s,id=GetID()
function s.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_DRAW)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
    e1:SetCountLimit(1,id,EFFECT_COUNT_CODE_OATH)
    e1:SetCondition(s.condition)
	e1:SetTarget(s.target)
	e1:SetOperation(s.activate)
	c:RegisterEffect(e1)
end

function s.filter(c)
    return c:IsCode(12306) and c:IsFaceup()
end

function s.condition(e,tp,eg,ep,ev,re,r,rp)
    return Duel.IsExistingMatchingCard(s.filter,tp,LOCATION_ONFIELD,0,1,nil)
end

function s.target(e,tp,eg,ep,ev,re,r,rp,chk)
    if chk==0 then return Duel.IsPlayerCanDraw(tp,2) and Duel.IsPlayerCanDraw(1-tp,2) end
    Duel.SetOperationInfo(0,CATEGORY_DRAW,nil,0,PLAYER_ALL,2)
end

function s.activate(e,tp,eg,ep,ev,re,r,rp)
    local fg1 = Duel.GetFieldGroup(tp,LOCATION_HAND,0)
    local fg2 = Duel.GetFieldGroup(1-tp,LOCATION_HAND,0)
    local ct1 = 2-#fg1
    local ct2 = 2-#fg2
    if ct1>0 then
        Duel.Draw(tp,ct1,REASON_EFFECT)
    end
    if ct2>0 then
        Duel.Draw(1-tp,ct2,REASON_EFFECT)
    end
end