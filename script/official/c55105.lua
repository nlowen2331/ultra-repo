
local s,id=GetID()
function s.initial_effect(c)
	--special summon
    c:EnableUnsummonable()
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_SPSUMMON_PROC)
	e1:SetProperty(EFFECT_FLAG_UNCOPYABLE)
	e1:SetRange(LOCATION_HAND)
	e1:SetCondition(s.spcon)
	e1:SetTarget(s.sptg)
	e1:SetOperation(s.spop)
	c:RegisterEffect(e1)
    --destroy replace
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_SINGLE)
	e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e2:SetRange(LOCATION_MZONE)
	e2:SetCountLimit(1)
	e2:SetCode(EFFECT_DESTROY_REPLACE)
	e2:SetTarget(s.desreptg)
	c:RegisterEffect(e2)
    -- 
    local e3=Effect.CreateEffect(c)
    e3:SetDescription(aux.Stringid(id,0))
    e3:SetCategory(CATEGORY_SPECIAL_SUMMON+CATEGORY_DAMAGE+CATEGORY_TOKEN)
    e3:SetType(EFFECT_TYPE_TRIGGER_O+EFFECT_TYPE_FIELD)
    e3:SetCode(EVENT_PHASE+PHASE_END)
    e3:SetRange(LOCATION_MZONE)
    e3:SetCountLimit(1)
	e3:SetCondition(s.tkncon)
    e3:SetTarget(s.tkntg)
    e3:SetOperation(s.tknop)
    c:RegisterEffect(e3)
end

function s.spcon(e,c)
	if c==nil then return true end
	return Duel.CheckReleaseGroup(c:GetControler(),Card.IsSetCard,1,false,1,true,c,c:GetControler(),nil,false,nil,0xd2)
end
function s.sptg(e,tp,eg,ep,ev,re,r,rp,c)
	local g=Duel.SelectReleaseGroup(tp,Card.IsSetCard,1,1,false,true,true,c,nil,nil,false,nil,0xd2)
	if g then
		g:KeepAlive()
		e:SetLabelObject(g)
	return true
	end
	return false
end
function s.spop(e,tp,eg,ep,ev,re,r,rp,c)
	local g=e:GetLabelObject()
	if not g then return end
	Duel.Release(g,REASON_COST)
	g:DeleteGroup()
end

function s.desreptg(e,tp,eg,ep,ev,re,r,rp,chk)
	local c=e:GetHandler()
	if chk==0 then return (c:IsReason(REASON_BATTLE) or rp~=tp) and not c:IsReason(REASON_REPLACE) and Duel.IsExistingMatchingCard(s.tfilter, tp, LOCATION_MZONE, 0, 1, e:GetHandler(),e) end
	if Duel.SelectEffectYesNo(tp,e:GetHandler(),96) then
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_DESREPLACE)
		local g=Duel.SelectMatchingCard(tp, s.tfilter, tp, LOCATION_MZONE, 0, 1, 1,e:GetHandler(),e)
		Duel.Destroy(g, REASON_EFFECT+REASON_REPLACE)
		return true
	else return false end
end
function s.tfilter(c,e)
	return  not c:IsImmuneToEffect(e) and not c:IsStatus(STATUS_DESTROY_CONFIRMED)
		and c:IsCode(55115) and c:IsFaceup()
end

function s.tkncon(e,tp)
    return Duel.GetTurnPlayer()==tp
end

function s.tkntg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0 and 
        Duel.IsPlayerCanSpecialSummonMonster(tp,55115,0,TYPES_TOKEN,0,0,6,RACE_PSYCHIC,ATTRIBUTE_DARK)
     end
	Duel.SetOperationInfo(0,CATEGORY_TOKEN,nil,2,0,0)
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,2,tp,0)
end
function s.tknop(e,tp,eg,ep,ev,re,r,rp)
    local ft=Duel.GetLocationCount(tp,LOCATION_MZONE)
    local n = 0
	if ft>0 and Duel.IsPlayerCanSpecialSummonMonster(tp,55115,0,TYPES_TOKEN,0,0,2,RACE_PSYCHIC,ATTRIBUTE_DARK) then
		for i=1,2 do
			local token=Duel.CreateToken(tp,55115)
			Duel.SpecialSummonStep(token,0,tp,tp,false,false,POS_FACEUP)
            n = n+1
            if Duel.GetLocationCount(tp,LOCATION_MZONE)<1 then break end
            if i<2 and not Duel.SelectYesNo(tp, aux.Stringid(id, 1)) then break end
		end
		Duel.SpecialSummonComplete()
	end
	if n<1 then return end
	Duel.BreakEffect()
	Duel.Damage(tp,n*500,REASON_EFFECT)
end

