local s,id=GetID()

function s.initial_effect(c)
   --if this card attacks an opponent's monster, at the start of the damage step: banish alll cards your opponent controls in that monster's column.
    local e1=Effect.CreateEffect(c)
    e1:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F)
    e1:SetCode(EVENT_BATTLE_START)
    e1:SetCountLimit(1)
    e1:SetProperty(EFFECT_FLAG_DELAY+EFFECT_FLAG_NO_TURN_RESET)
    e1:SetCondition(s.condition)
    e1:SetOperation(s.operation)
    c:RegisterEffect(e1)
end

function s.condition(e,tp,eg,ep,ev,re,r,rp)
    local c=e:GetHandler()
    local at = Duel.GetAttacker()
    local bc=at:GetBattleTarget()
    e:SetLabelObject(bc)
    return c and at and at==c and bc and bc:IsControler(1-tp) and bc:IsRelateToBattle()
end

function s.filter(c,tp)
    return c:IsControler(tp) and c:IsLocation(LOCATION_MZONE)
end

function s.operation(e,tp,eg,ep,ev,re,r,rp)
    local c=e:GetHandler()
    local bc=e:GetLabelObject()
    local cg = bc:GetColumnGroup(1,1)
    local sg = cg:Filter(s.filter,nil,1-tp)
    sg:Merge(bc)
    if #sg>0 then
        Duel.Destroy(sg, REASON_EFFECT)
    end
    --change this card to defense position during the end phase
    local fid=c:GetFieldID()
    c:RegisterFlagEffect(id,RESET_EVENT+RESETS_STANDARD,0,1,fid)
    local e1=Effect.CreateEffect(c)
    e1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
    e1:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
    e1:SetCode(EVENT_PHASE+PHASE_END)
    e1:SetReset(RESET_PHASE+PHASE_END)
    e1:SetCountLimit(1)
    e1:SetLabel(fid)
    e1:SetLabelObject(c)
    e1:SetCondition(s.defcon)
    e1:SetOperation(s.defop)
    Duel.RegisterEffect(e1,tp)
end

function s.defcon(e,tp,eg,ep,ev,re,r,rp)
	local tc=e:GetLabelObject()
	if tc and tc:GetFlagEffectLabel(id)==e:GetLabel() and tc:IsOnField() and tc:IsAttackPos() then
		return true
	else
		e:Reset()
		return false
	end
end
function s.defop(e,tp,eg,ep,ev,re,r,rp)
	local tc=e:GetLabelObject()
	Duel.ChangePosition(tc, POS_FACEUP_DEFENSE)
end

