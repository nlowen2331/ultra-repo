--コミックハンド
--Comic Hand
local s,id=GetID()
function s.initial_effect(c)
	aux.AddEquipProcedure(c,1,s.filter,s.eqlimit,nil,s.target,s.operation)
	--Take control
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_EQUIP)
	e3:SetCode(EFFECT_SET_CONTROL)
	e3:SetValue(s.cval)
	c:RegisterEffect(e3)
	--Equipped monster is treated as a Toon
	local e4=Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_EQUIP)
	e4:SetCode(EFFECT_ADD_SETCODE)
	e4:SetValue(0x62)
	c:RegisterEffect(e4)
	--can't attack the turn this card was activated
    local e5=Effect.CreateEffect(c)
    e5:SetType(EFFECT_TYPE_EQUIP)
    e5:SetCode(EFFECT_CANNOT_ATTACK)
    e5:SetCondition(s.atkcon)
    c:RegisterEffect(e5)
	--cannot be target
	local e6=Effect.CreateEffect(c)
	e6:SetType(EFFECT_TYPE_SINGLE)
	e6:SetCode(EFFECT_CANNOT_BE_EFFECT_TARGET)
	e6:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e6:SetRange(LOCATION_SZONE)
	e6:SetCondition(s.tgcon)
	e6:SetValue(s.tgvalue)
	e6:SetReset(RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END)
	c:RegisterEffect(e6)
	--indes
	local e7=e6:Clone()
	e7:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
	e7:SetValue(s.indval)
	c:RegisterEffect(e7)
end
function s.target(e,tp,eg,ep,ev,re,r,rp,tc)
	Duel.SetOperationInfo(0,CATEGORY_CONTROL,tc,1,0,0)
end
function s.eqlimit(e,c)
	return e:GetHandlerPlayer()~=c:GetControler() or e:GetHandler():GetEquipTarget()==c
end
function s.cval(e,c)
	return e:GetHandlerPlayer()
end

function s.filter(c)
    return c:IsControlerCanBeChanged() and c:IsAttackBelow(2000)
end

function s.operation(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    c:RegisterFlagEffect(id, RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END, 0, 1)
end

function s.atkcon(e)
    return e:GetHandler():GetFlagEffect(id)~=0
end

function s.twfilter(c)
	return c:IsFaceup() and c:IsCode(12306)
end

function s.tgcon(e)
	return Duel.IsExistingMatchingCard(s.twfilter, e:GetHandlerPlayer(), LOCATION_FZONE, 0, 1, nil)
end

function s.indval(e,re,tp)
	return tp~=e:GetHandlerPlayer()
end

function s.tgvalue(e,re,rp)
	return rp~=e:GetHandlerPlayer()
end