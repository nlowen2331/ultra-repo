--時空の落とし穴
--Time-Space Trap Hole
local s,id=GetID()
function s.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_SPSUMMON_SUCCESS)
	e1:SetTarget(s.target)
	e1:SetOperation(s.activate)
	c:RegisterEffect(e1)
end

function s.filter(c,e,tp,lv)
    return c:IsSetCard(0x62) and c:IsCanBeSpecialSummoned(e,0,tp,false,false)
        and c:IsLevel(lv)
end
function s.filter2(c,e,tp,g)
    return c:IsSetCard(0x62) and c:IsCanBeSpecialSummoned(e,0,tp,false,false)
        and g:FilterCount(Card.IsLevel, nil, c:GetLevel())>0
end
function s.filter3(c,e,tp)
    return c:IsSummonPlayer(tp)
end
function s.chkfilter(c,e,tp)
    return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil, e,tp,c:GetLevel())
        and c:IsSummonPlayer(1-tp)
end
function s.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return eg and eg:FilterCount(s.chkfilter, nil, e,tp)>0
		and Duel.GetLocationCount(tp, LOCATION_MZONE)>0 and Duel.GetFlagEffect(tp,id)==0 
	end
	Duel.RegisterFlagEffect(tp, id, RESET_CHAIN, 0, 1)
end

function s.activate(e,tp,eg,ep,ev,re,r,rp)
    if not eg or Duel.GetLocationCount(tp, LOCATION_MZONE)<1 then return end
	local g=eg:Filter(s.filter3, nil,e,1-tp)
    if #g<1 then return end
	local dcount=Duel.GetFieldGroupCount(tp,LOCATION_DECK,0)
    local g2 = Duel.GetMatchingGroup(s.filter2, tp, LOCATION_DECK, 0, nil, e,tp,g)
	local seq=-1
	local tc=g2:GetFirst()
	local pivot=nil
	for tc in aux.Next(g2) do
		if tc:GetSequence()>seq then 
			seq=tc:GetSequence()
			pivot=tc
		end
	end
	if seq==-1 then
		Duel.ConfirmDecktop(tp,dcount)
		Duel.ShuffleDeck(tp)
		return
	end
	Duel.ConfirmDecktop(tp,dcount-seq)
    if pivot:IsCanBeSpecialSummoned(e,0,tp,false,false) then
        Duel.SpecialSummon(pivot, 0, tp, tp, false, false, POS_FACEUP)
    end
    Duel.ShuffleDeck(tp)
end
