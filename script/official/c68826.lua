--trap card with this effect: Both players choose 1 of their face-up monsters to shuffle into the Deck.
local s, id = GetID()
function s.initial_effect(c)
    --Activate
    local e1=Effect.CreateEffect(c)
    e1:SetCategory(CATEGORY_TODECK)
    e1:SetType(EFFECT_TYPE_ACTIVATE)
    e1:SetCode(EVENT_FREE_CHAIN)
    e1:SetProperty(EFFECT_FLAG_CARD_TARGET)
    e1:SetHintTiming(0,TIMING_SUMMON+TIMING_SPSUMMON+TIMING_FLIPSUMMON+TIMING_END_PHASE)
    e1:SetTarget(s.target)
    e1:SetOperation(s.activate)
    c:RegisterEffect(e1)
end

function s.filter(c)
    return c:IsFaceup() and c:IsAbleToDeck()
end

function s.filter2(c)
    return c:IsFaceup() and c:IsAbleToDeck() and c:IsSetCard(0x71)
end

function s.target(e,tp,eg,ep,ev,re,r,rp,chk)
    if chk==0 then return Duel.IsExistingTarget(s.filter2, tp, LOCATION_MZONE, 0 , 1, nil) 
        and Duel.IsExistingTarget(s.filter, 1-tp, LOCATION_MZONE, 0, 1, nil) end
    Duel.SetOperationInfo(0, CATEGORY_TODECK, nil, 2, 0, 0)
end

function s.activate(e,tp,eg,ep,ev,re,r,rp)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
    local g1=Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1,nil)
    Duel.Hint(HINT_SELECTMSG, 1-tp, HINTMSG_TODECK)
    local g2=Duel.SelectMatchingCard(1-tp, s.filter, 1-tp, LOCATION_MZONE, 0, 1, 1,nil)
    g1:Merge(g2)
    if #g1>0 then
        Duel.SendtoDeck(g1, nil, 2, REASON_EFFECT)
    end
end