local s, id = GetID()
function s.initial_effect(c)
    --get poly
	local e5=Effect.CreateEffect(c)
	e5:SetDescription(aux.Stringid(id,0))
	e5:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e5:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e5:SetProperty(EFFECT_FLAG_DELAY)
	e5:SetCode(EVENT_TO_HAND)
	e5:SetCountLimit(1,id)
	e5:SetCondition(s.thcon)
    e5:SetCost(s.thcost)
	e5:SetTarget(s.thtg)
	e5:SetOperation(s.thop)
	c:RegisterEffect(e5)
    --If this card is Normal/Special Summoned: You can target 1 other face-up monster on the field; switch this card's original ATK/DEF with that target's until the end of the turn.
    local e6=Effect.CreateEffect(c)
    e6:SetDescription(aux.Stringid(id,1))
    e6:SetCategory(CATEGORY_ATKCHANGE+CATEGORY_DEFCHANGE)
    e6:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
    e6:SetCode(EVENT_SUMMON_SUCCESS)
    e6:SetProperty(EFFECT_FLAG_CARD_TARGET+EFFECT_FLAG_DELAY)
    e6:SetTarget(s.atktg)
    e6:SetOperation(s.atkop)
    c:RegisterEffect(e6)
    local e7=e6:Clone()
    e7:SetCode(EVENT_SPSUMMON_SUCCESS)
    c:RegisterEffect(e7)
end

function s.thcon(e,tp,eg,ep,ev,re,r,rp)
    local c = e:GetHandler()
	if not re then return false end
    local rc = re:GetHandler()
	return c:IsPreviousLocation(LOCATION_DECK) and c:IsPreviousControler(tp)
        and rc:IsSetCard(0xf3)
end

function s.thfilter(c)
    return c:IsCode(62028) and c:IsAbleToHand()
end

--discard this card for thcost
function s.thcost(e,tp,eg,ep,ev,re,r,rp,chk)
    local c = e:GetHandler()
    if chk==0 then return c:IsDiscardable() end
    Duel.SendtoGrave(c, REASON_COST+REASON_DISCARD)
end

function s.thtg(e,tp,eg,ep,ev,re,r,rp,chk)
    if chk==0 then return Duel.IsExistingMatchingCard(s.thfilter,tp,LOCATION_DECK,0,1,nil) end
    Duel.SetOperationInfo(0,CATEGORY_TOHAND,nil,1,tp,LOCATION_DECK)
end

function s.thop(e,tp,eg,ep,ev,re,r,rp)
    Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_ATOHAND)
    local g=Duel.SelectMatchingCard(tp,s.thfilter,tp,LOCATION_DECK,0,1,1,nil)
    if #g>0 then
        Duel.SendtoHand(g,nil,REASON_EFFECT)
        Duel.ConfirmCards(1-tp,g)
    end
end

function s.sfilter(c,att,def)
    return c:GetBaseAttack()~=att or c:GetBaseDefense()~=def
end

function s.atktg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
    local c = e:GetHandler()
    local att = c:GetBaseAttack()
    local def = c:GetBaseDefense()
    if chkc then return chkc:IsFaceup() and chkc~=e:GetHandler() and (chkc:GetBaseAttack()~=att or chkc:GetBaseDefense()~=def) end
    if chk==0 then return Duel.IsExistingTarget(Card.IsFaceup,tp,LOCATION_MZONE,LOCATION_MZONE,1,e:GetHandler(),att,def) end
    Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FACEUP)
    Duel.SelectTarget(tp,Card.IsFaceup,tp,LOCATION_MZONE,LOCATION_MZONE,1,1,e:GetHandler(),att,def)
end

function s.atkop(e,tp,eg,ep,ev,re,r,rp)
	local c = e:GetHandler()
    local tc = Duel.GetFirstTarget()
    local att = c:GetBaseAttack()
    local def = c:GetBaseDefense()
    local tcatt = tc:GetBaseAttack()
    local tcdef = tc:GetBaseDefense()
	if tc and tc:IsFaceup() and c and c:IsFaceup() and tc:IsRelateToEffect(e) and c:IsRelateToEffect(e) then
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_SET_BASE_ATTACK)
		e1:SetValue(tcatt)
		e1:SetReset(RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END)
		c:RegisterEffect(e1)
        local e2=Effect.CreateEffect(c)
        e2:SetType(EFFECT_TYPE_SINGLE)
        e2:SetCode(EFFECT_SET_BASE_DEFENSE)
        e2:SetValue(tcdef)
        e2:SetReset(RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END)
        c:RegisterEffect(e2)
		local e3=Effect.CreateEffect(c)
		e3:SetType(EFFECT_TYPE_SINGLE)
		e3:SetCode(EFFECT_SET_BASE_ATTACK)
		e3:SetValue(att)
		e3:SetReset(RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END)
		tc:RegisterEffect(e3)
        local e4=Effect.CreateEffect(c)
        e4:SetType(EFFECT_TYPE_SINGLE)
        e4:SetCode(EFFECT_SET_BASE_DEFENSE)
        e4:SetValue(def)
        e4:SetReset(RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END)
        tc:RegisterEffect(e4)
	end
end