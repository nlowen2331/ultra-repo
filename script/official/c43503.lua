--幻獣機ヤクルスラーン
local s,id=GetID()
function s.initial_effect(c)
	--synchro summon
	Synchro.AddProcedure(c,aux.FilterBoolFunctionEx(Card.IsSetCard,0x33),1,1,Synchro.NonTunerEx(aux.TRUE,nil),1,99)
	c:EnableReviveLimit()
    --c:RegisterEffect(e3)
    local e1=Effect.CreateEffect(c)
    e1:SetType(EFFECT_TYPE_SINGLE)
    e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
    e1:SetRange(LOCATION_MZONE)
    e1:SetCode(EFFECT_INDESTRUCTABLE_COUNT)
    e1:SetCountLimit(1)
    e1:SetValue(1)
    e1:SetCondition(s.descon)
    c:RegisterEffect(e1)
    --
	local e2=Effect.CreateEffect(c)
	e2:SetCategory(CATEGORY_DESTROY)
	e2:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F)
	e2:SetCode(EVENT_BATTLED)
	e2:SetCondition(s.descon2)
	e2:SetOperation(s.desop2)
    c:RegisterEffect(e2)
end
function s.desfilter(c)
    return c:IsFaceup() and c:IsSetCard(0x33)
end
function s.descon(e)
    return Duel.IsExistingMatchingCard(s.desfilter,e:GetHandlerPlayer(),LOCATION_MZONE,0,1,e:GetHandler())
end
function s.descon2(e,tp,eg,ep,ev,re,r,rp)
	local att= Duel.GetAttacker()
    local bt = att:GetBattleTarget()
	return att and bt
end

function s.desop2(e,tp,eg,ep,ev,re,r,rp)
	local att = Duel.GetAttacker()
    local bt = att:GetBattleTarget()
    local c = e:GetHandler()
    local a,b
    if att==c then
        a=att
        b=bt
    else
        b=att
        a=bt
    end
	if not b or not b:IsRelateToBattle() or not b:IsControler(1-tp) then return false end
	Duel.Destroy(b,REASON_EFFECT)
end