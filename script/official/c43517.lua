--BF－上弦のピナーカ
local s,id=GetID()
function s.initial_effect(c)
    --if this card iis sent from the field to the GY: You can banish it; SPecial Summon 2 Steam Tokens
    local e1=Effect.CreateEffect(c)
    e1:SetCategory(CATEGORY_SPECIAL_SUMMON+CATEGORY_TOKEN)
    e1:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
    e1:SetCode(EVENT_TO_GRAVE)
	e1:SetProperty(EFFECT_FLAG_DELAY)
    e1:SetCondition(s.condition)
    e1:SetCost(aux.bfgcost)
    e1:SetTarget(s.target)
    e1:SetOperation(s.operation)
    c:RegisterEffect(e1)
end

function s.condition(e,tp,eg,ep,ev,re,r,rp)
    local c = e:GetHandler()
    return c:IsPreviousLocation(LOCATION_ONFIELD)
end

function s.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0 and 
		Duel.IsPlayerCanSpecialSummonMonster(tp,43539,0,TYPES_TOKEN+TYPE_TUNER,0,0,2,RACE_AQUA,ATTRIBUTE_WATER) end
	Duel.SetOperationInfo(0,CATEGORY_TOKEN,nil,2,0,0)
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,2,tp,0)
end
function s.operation(e,tp,eg,ep,ev,re,r,rp)
	local ft = Duel.GetLocationCount(tp,LOCATION_MZONE)
	if ft>0 and Duel.IsPlayerCanSpecialSummonMonster(tp,43539,0,TYPES_TOKEN+TYPE_TUNER,0,0,1,RACE_AQUA,ATTRIBUTE_WATER) then
		local token=Duel.CreateToken(tp,43539)
		if not Duel.SpecialSummonStep(token,0,tp,tp,false,false,POS_FACEUP) then return end
		local e1=Effect.CreateEffect(e:GetHandler())
		e1:SetDescription(3303)
		e1:SetProperty(EFFECT_FLAG_CLIENT_HINT)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UNRELEASABLE_SUM)
		e1:SetValue(1)
		e1:SetReset(RESET_EVENT+RESETS_STANDARD)
		token:RegisterEffect(e1,true)
		local e2=e1:Clone()
		e2:SetCode(EFFECT_UNRELEASABLE_NONSUM)
		token:RegisterEffect(e2,true)
		--Cannot be used as synchro material
		local e3=e2:Clone()
		e3:SetDescription(3310)
		e3:SetValue(s.synlimit)
		e3:SetCode(EFFECT_CANNOT_BE_SYNCHRO_MATERIAL)
		token:RegisterEffect(e3,true)
		--if i<2 and not Duel.SelectYesNo(tp, aux.Stringid(id, 0)) then break end
		Duel.SpecialSummonComplete()
	end
end

function s.synlimit(e,c)
	if not c then return false end
	return not c:IsSetCard(0x33)
end