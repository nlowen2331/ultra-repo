
local s,id=GetID()
function s.initial_effect(c)
    --if you control no momsters, you can special summon this card from your hand
    local e1=Effect.CreateEffect(c)
    e1:SetCode(EFFECT_SPSUMMON_PROC)
    e1:SetType(EFFECT_TYPE_FIELD)
    e1:SetProperty(EFFECT_FLAG_UNCOPYABLE)
    e1:SetRange(LOCATION_HAND)
    e1:SetCondition(s.spcon)
    c:RegisterEffect(e1)
    --
    local e2=Effect.CreateEffect(c)
    e2:SetCategory(CATEGORY_TOKEN+CATEGORY_SPECIAL_SUMMON)
    e2:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
    e2:SetCode(EVENT_SPSUMMON_SUCCESS)
    e2:SetProperty(EFFECT_FLAG_DELAY)
    e2:SetCountLimit(1,id)
    e2:SetTarget(s.tkntg)
    e2:SetOperation(s.tknop)
    c:RegisterEffect(e2)
    local e3=e2:Clone()
    e3:SetCode(EVENT_SUMMON_SUCCESS)
    c:RegisterEffect(e3)
end

function s.spcon(e,c)
    if c==nil then return true end
    return Duel.GetLocationCount(c:GetControler(),LOCATION_MZONE)>0
        and not Duel.IsExistingMatchingCard(aux.TRUE,c:GetControler(),LOCATION_MZONE,0,1,nil)
end

--filter for discardable blackwing monster
function s.filter(c)
    return c:IsDiscardable() and c:IsSetCard(0x33) and c:IsType(TYPE_MONSTER)
end

function s.tkntg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0 and 
        Duel.IsPlayerCanSpecialSummonMonster(tp,43538,0,TYPES_TOKEN,0,0,2,RACE_WINGEDBEAST,ATTRIBUTE_DARK)
        and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND, 0, 1, nil)
     end
	Duel.SetOperationInfo(0,CATEGORY_TOKEN,nil,2,0,0)
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,2,tp,0)
end
function s.tknop(e,tp,eg,ep,ev,re,r,rp)
    local ft=Duel.GetLocationCount(tp,LOCATION_MZONE)
    if Duel.DiscardHand(tp, s.filter, 1, 1, REASON_EFFECT, nil)<1 then return end
	if ft>0 and Duel.IsPlayerCanSpecialSummonMonster(tp,43538,0,TYPES_TOKEN,0,0,2,RACE_WINGEDBEAST,ATTRIBUTE_DARK) then
		for i=1,2 do
			local token=Duel.CreateToken(tp,43538)
			Duel.SpecialSummonStep(token,0,tp,tp,false,false,POS_FACEUP)
            local e1=Effect.CreateEffect(e:GetHandler())
			e1:SetDescription(3303)
			e1:SetProperty(EFFECT_FLAG_CLIENT_HINT)
			e1:SetType(EFFECT_TYPE_SINGLE)
			e1:SetCode(EFFECT_UNRELEASABLE_SUM)
			e1:SetValue(1)
			e1:SetReset(RESET_EVENT+RESETS_STANDARD)
			token:RegisterEffect(e1,true)
			local e2=e1:Clone()
			e2:SetCode(EFFECT_UNRELEASABLE_NONSUM)
			token:RegisterEffect(e2,true)
            --Cannot be used as synchro material
			local e3=e2:Clone()
			e3:SetDescription(3310)
            e3:SetValue(s.synlimit)
			e3:SetCode(EFFECT_CANNOT_BE_SYNCHRO_MATERIAL)
			token:RegisterEffect(e3,true)
            if Duel.GetLocationCount(tp,LOCATION_MZONE)<1 then break end
            if i<2 and not Duel.SelectYesNo(tp, aux.Stringid(id, 0)) then break end
		end
		Duel.SpecialSummonComplete()
	end
end

function s.synlimit(e,c)
	if not c then return false end
	return not c:IsSetCard(0x33)
end