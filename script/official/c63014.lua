
local s,id=GetID()
function s.initial_effect(c)
	--activate
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	c:RegisterEffect(e1)
	--atk up
	local e4=Effect.CreateEffect(c)
	e4:SetDescription(aux.Stringid(id,0))
	e4:SetType(EFFECT_TYPE_FIELD)
	e4:SetCode(EFFECT_UPDATE_ATTACK)
	e4:SetRange(LOCATION_SZONE)
	e4:SetTargetRange(LOCATION_MZONE,0)
	e4:SetTarget(s.atktg2)
	e4:SetCondition(s.atkcon2)
	e4:SetValue(500)
	c:RegisterEffect(e4)
end

function s.atktg2(e,c)
	return c:IsSetCard(0x4) and Duel.GetTurnPlayer()==e:GetHandlerPlayer() 
		and Duel.GetAttacker()==c
end
function s.atkcon2(e)
	return Duel.GetCurrentPhase()==PHASE_DAMAGE_CAL
end