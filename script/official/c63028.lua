--ドラゴンメイド・シュトラール
--Dragonmaid Strahl
--Scripted by King Yamato
local s,id=GetID()
function s.initial_effect(c)
	c:EnableReviveLimit()
	Fusion.AddProcMixN(c,true,true,s.mfilter1,1,s.mfilter2,1)
	--
    local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_EXTRA_ATTACK)		
	e1:SetValue(1)
	c:RegisterEffect(e1)
	--this card is unaffected by your opponent's card effects the turn it's Special Summoned
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_IMMUNE_EFFECT)
	e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e2:SetRange(LOCATION_MZONE)
	e2:SetCondition(s.econ)
	e2:SetValue(s.efilter)
	c:RegisterEffect(e2)
	--
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_ATKCHANGE)
	e1:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F)
	e1:SetProperty(EFFECT_FLAG_DELAY)
	e1:SetCode(EVENT_BATTLE_DAMAGE)
	e1:SetCondition(s.atkcon)
	e1:SetOperation(s.atkop)
	c:RegisterEffect(e1)
end
function s.mfilter1(c,fc,sumtype,tp)
	return c:IsCode(63006)
end
function s.mfilter2(c,fc,sumtype,tp)
	return c:IsSetCard(0x4,fc,sumtype,tp) and c:IsRace(RACE_WARRIOR,fc,sumtype,tp)
end

function s.econ(e)
	local c = e:GetHandler()
	return c:IsStatus(STATUS_SPSUMMON_TURN)
end

function s.efilter(e,te)
	return te:GetOwnerPlayer()~=e:GetHandlerPlayer()
end

function s.atkcon(e,tp,eg,ep,ev,re,r,rp)
	return ep~=tp
end

function s.atkop(e,tp,eg,ep,ev,re,r,rp)
	--for the rest of this turn, all "Amazones" monsters you control gain 300 ATK
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetTargetRange(LOCATION_MZONE,0)
	e1:SetTarget(s.atktg)
	e1:SetValue(300)
	e1:SetReset(RESET_PHASE+PHASE_END)
	Duel.RegisterEffect(e1,tp)
end

function s.atktg(e,c)
	return c:IsSetCard(0x4)
end

