
local s,id=GetID()

function s.initial_effect(c)
	aux.AddEquipProcedure(c,0,s.eqfilter,nil,nil,nil,nil,nil,id)
    --atk
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_EQUIP)
	e3:SetCode(EFFECT_UPDATE_ATTACK)
	e3:SetCondition(s.atkcon)
	e3:SetValue(1500)
	c:RegisterEffect(e3)
end
function s.eqfilter(c)
	return c:IsFaceup() and c:IsSetCard(0xe)
end

function s.atkcon(e)
	local c = e:GetHandler()
	local eq = c:GetEquipTarget()
	return Duel.GetCurrentPhase()==PHASE_DAMAGE_CAL and Duel.GetAttackTarget()==nil and eq==Duel.GetAttacker()
end