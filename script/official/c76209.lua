
local s,id=GetID()
function s.initial_effect(c)
	c:EnableReviveLimit()
	Fusion.AddProcMixN(c,true,true,s.mfilter,2)
    --take control
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_QUICK_O)
	e1:SetCode(EVENT_CHAINING)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCountLimit(1)
	e1:SetCondition(s.tccon)
	e1:SetTarget(s.tctg)
	e1:SetOperation(s.tcop)
	c:RegisterEffect(e1)
	--destroy replace
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCountLimit(1)
	e1:SetCode(EFFECT_DESTROY_REPLACE)
	e1:SetTarget(s.desreptg)
	c:RegisterEffect(e1)
end
function s.mfilter(c,fc,sumtype,tp)
	return c:IsSetCard(0x10f3,fc,sumtype,tp)
end

function s.tccon(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local rc=re:GetHandler()
	return  rc:GetControler()~=tp
		and re:IsActiveType(TYPE_MONSTER) and re:GetActivateLocation()==LOCATION_MZONE
		and rc:IsControlerCanBeChanged() and rc:IsOnField()
end

function s.tctg(e,tp,eg,ep,ev,re,r,rp,chk)
    local c=e:GetHandler()
	local rc=re:GetHandler()
	if chk==0 then return rc:IsOnField() and rc:IsCanBeEffectTarget(e) end
	Duel.SetTargetCard(rc)
	Duel.SetOperationInfo(0,CATEGORY_CONTROL,rc,1,0,0)
end
function s.tcop(e,tp,eg,ep,ev,re,r,rp)
	local tc=Duel.GetFirstTarget()
	if tc and tc:IsRelateToEffect(e) and tc:IsOnField() then
		Duel.GetControl(tc,tp,PHASE_END,2)
	end
end


function s.repfilter(c)
	return c:IsReleasableByEffect() and c:IsSetCard(0x10f3) and c:IsFaceup()
end
function s.desreptg(e,tp,eg,ep,ev,re,r,rp,chk)
	local c=e:GetHandler()
	if chk==0 then return (c:IsReason(REASON_BATTLE) or rp~=tp) and Duel.CheckReleaseGroup(tp,s.repfilter,1,c)
	and not c:IsReason(REASON_REPLACE) end
	if Duel.SelectYesNo(tp, aux.Stringid(id, 0)) then
		local g=Duel.SelectReleaseGroup(tp,s.repfilter,1,1,c)
		Duel.Release(g,REASON_EFFECT+REASON_REPLACE)
		return true
	else return false end
end

